from Graph import Graph, HyperGraph
from Simulation import MoteCarlo
from time import time
import os
import sys
import multiprocessing as mp
from joblib import Parallel, delayed
import time

def greedy(g,sources, budget, num_simulation,outFile):
    #g=Graph()
    hg=HyperGraph()
    mt=MoteCarlo()
    nodeUs=g.nodes
    visit=[False]*(g.numNodes+1)
    visit_mark=[0]*(g.numNodes+1)
    for source in sources:
        visit[source]=True
        nodeUs.remove(source)
    
    for i in range(g.numNodes+1):
        if(g.degree[i]==0):
            if(i in nodeUs):
                nodeUs.remove(i)
    
    A=[]
    maxInf=0
    
    estimate=mt.motecarlo_simulation(g,sources,num_simulation, hg, visit, visit_mark)
    numNodeA=0
    numNodeUs=len(nodeUs)
    #print(nodeUs)
    start=time.time()
    times=[]
    while(numNodeA<budget+1):
        maxNode=nodeUs[0]
        for node in nodeUs:
            visit[node]=True
            #curNode=node
            tmpEs=mt.motecarlo_simulation(g,sources,num_simulation, hg, visit, visit_mark)
            tmpMax=estimate-tmpEs
            if(tmpMax>maxInf):
                maxInf=tmpMax
                maxNode=node
                estimate=tmpEs
            visit[node]=False

            #print(maxNode)

        A.append(maxNode)
        nodeUs.remove(maxNode)
        '''for i in range(len(nodeUs)):
            if(nodeUs[i]==maxNode):
                print("OK")'''
        visit[maxNode]=False

        print(maxNode)

        numNodeA+=1
        if(numNodeA%10==0 and numNodeA>0):
            end=time.time()-start
            times.append(end)
            writeFile=outFile+"seed_"+str(numNodeA)+".a"
            timeFile=outFile+"time_"+str(numNodeA)+".txt"
            writetoFile(timeFile,times)
            writetoFile(writeFile,A)
        #print(maxInf)
    #timeFile=outFile+"times.txt"
    #writetoFile(timeFile,times)
    print("End Greedy")
        
def read_source(file_name):
    f = open(file_name, 'r')
    data=f.readlines()
    sources=[]
    sources=[int(i.strip()) for i in data]
    return sources

def writetoFile(file_name,nodeList):
    f=open(file_name,'w')
    for node in nodeList:
        f.writelines(str(node)+"\n")
    f.close()

def main():
    data_name=sys.argv[1]

    static_dir="../data"
    graph_file_name=static_dir+"/"+data_name+"/"+"network.txt"
    source_file=static_dir+"/"+data_name+"/"+"network.seeds"
    result_dir=static_dir+"/"+data_name+"/"+"result/"

    g=Graph()

    g.readGraphLT(graph_file_name)
    sources=read_source(source_file)
    result_dir

    print("Number of nodes: "+ str(g.numNodes))
    print("Number of edges: "+ str(g.numEdges))

    #greedy(g,sources,10,1, result_dir)
    jobs=[]
    num_core=mp.cpu_count()
    p=mp.Pool(processes=num_core)
    start=time.time()
    p=mp.Process(target=greedy, args=(g,sources,100,1,result_dir))
    jobs.append(p)
    p.start()

    #writetoFile("network.seeds",[1,2])

if __name__ == '__main__':
    main()

    

