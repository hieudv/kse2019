from Graph import Graph, HyperGraph
#from numba import autojit, prange
import multiprocessing as mp
from joblib import Parallel, delayed
import time
import sys

class MoteCarlo(object):

    def __init__(self):
        pass

    def motecarlo_simulation(self, g,sources,num_simulation, hg, visit, visit_mark):
        #start=time.time()
        num_sources=len(sources)
        #print(sources)
        for s in sources:
            #print(s)
            visit[s]=True
        for i in range(num_sources):
            visit_mark[i]=sources[i]

        total_node=0
        for i in range(num_simulation):
            total_node+=hg.pollingLT1(g,sources,visit, visit_mark)
        
        total_node=total_node/num_simulation

        return total_node

    def simulation(self,g,sources, num_simulation,A):
        hg=HyperGraph()
        mt=MoteCarlo()
        nodeUs=g.nodes
        visit=[False]*(g.numNodes+1)
        visit_mark=[0]*(g.numNodes+1)
        for source in sources:
            visit[source]=True
            #nodeUs.remove(source)

        for a in A:
            visit[a]=True

        estimate=mt.motecarlo_simulation(g,sources,num_simulation, hg, visit, visit_mark)
        
        return estimate

    def read_source(self,file_name):
        f = open(file_name, 'r')
        data=f.readlines()
        sources=[]
        sources=[int(i.strip()) for i in data]
        return sources

    def writetoFile(self,file_name,nodeList):
        f=open(file_name,'w')
        for node in nodeList:
            f.writelines(str(node)+"\n")
        f.close()

def main():

    data_name=sys.argv[1]
    alg=sys.argv[2]

    static_dir="../data"
    graph_file_name=static_dir+"/"+data_name+"/"+"network.txt"
    source_file=static_dir+"/"+data_name+"/"+"network.seeds"
    result_dir=static_dir+"/"+data_name+"/"+"result/"
    g=Graph()
    hg=HyperGraph()
    mt=MoteCarlo()

    g.readGraphLT(graph_file_name)
    sources=mt.read_source(source_file)

    print("Number of nodes: "+ str(g.numNodes))
    print("Number of edges: "+ str(g.numEdges))

    seedGreedy=["seed_10.a","seed_20.a","seed_30.a","seed_40.a","seed_50.a","seed_60.a","seed_70.a","seed_80.a","seed_90.a","seed_100.a" ]
    seedSGA=["seed_10.sga","seed_20.sga","seed_30.sga","seed_40.sga","seed_50.sga","seed_60.sga","seed_70.sga","seed_80.sga","seed_90.sga","seed_100.sga" ]
    seedDegree=["seed_10.dgree","seed_20.dgree","seed_30.dgree","seed_40.dgree","seed_50.dgree","seed_60.dgree","seed_70.dgree","seed_80.dgree","seed_90.dgree","seed_100.dgree" ]
    seedRandom=["seed_10.random","seed_20.random","seed_30.random","seed_40.random","seed_50.random","seed_60.random","seed_70.random","seed_80.random","seed_90.random","seed_100.random"]
    seed_name_arr=[]
    if alg=="SGA":
        seed_name_arr=seedSGA
    elif alg=="Greedy":
        seed_name_arr=seedGreedy
    elif alg=="Degree":
        seed_name_arr=seedDegree
    elif alg=="Random":
        seed_name_arr=seedRandom
    else:
        print("Wrong input algorithm!")
    
    es_arr=[]
    A=[]
    es_without_A=mt.simulation(g,sources,10000,A)
    
    for seed_name in seed_name_arr:
        file_name=result_dir+seed_name
        A=mt.read_source(file_name)
        es=mt.simulation(g,sources,10000,A)
        es_result=es_without_A-es
        print(es_result)
        es_arr.append(es_result)
    
    outFile=result_dir+"results_"+alg+ ".txt"
    mt.writetoFile(outFile,es_arr)

    #si.process()
if __name__ == '__main__':
    main()
    


    