from Graph import Graph, HyperGraph
from Simulation import MoteCarlo
from time import time
import os
import sys
import multiprocessing as mp
from joblib import Parallel, delayed
import time
import numpy as np

def greedy(g,sources, budget, num_simulation,outFile):
    #g=Graph()
    hg=HyperGraph()
    mt=MoteCarlo()
    nodeUs=g.nodes
    visit=[False]*(g.numNodes+1)
    visit_mark=[0]*(g.numNodes+1)
    for source in sources:
        visit[source]=True
        nodeUs.remove(source)
    
    for i in range(g.numNodes+1):
        if(g.degree[i]==0):
            if(i in nodeUs):
                nodeUs.remove(i)
    
    A=[]
    maxInf=0
    
    estimate=mt.motecarlo_simulation(g,sources,num_simulation, hg, visit, visit_mark)
    numNodeA=0
    numNodeUs=len(nodeUs)
    #print(nodeUs)
    start=time.time()
    times=[]
    while(numNodeA<budget+1):
        maxNode=nodeUs[0]
        for node in nodeUs:
            visit[node]=True
            #curNode=node
            tmpEs=mt.motecarlo_simulation(g,sources,num_simulation, hg, visit, visit_mark)
            tmpMax=estimate-tmpEs
            if(tmpMax>maxInf):
                maxInf=tmpMax
                maxNode=node
                estimate=tmpEs
            visit[node]=False

            #print(maxNode)

        A.append(maxNode)
        nodeUs.remove(maxNode)
        '''for i in range(len(nodeUs)):
            if(nodeUs[i]==maxNode):
                print("OK")'''
        visit[maxNode]=False

        print(maxNode)

        numNodeA+=1
        if(numNodeA%10==0 and numNodeA>0):
            end=time.time()-start
            times.append(end)
            writeFile=outFile+"seed_"+str(numNodeA)+".a"
            timeFile=outFile+"time_"+str(numNodeA)+".txt"
            writetoFile(timeFile,times)
            writetoFile(writeFile,A)
        #print(maxInf)
    #timeFile=outFile+"times.txt"
    #writetoFile(timeFile,times)
    print("End Greedy")

def SGA(g,sources, budget, num_live, hg, outFile):
    #g=Graph()
    #hg=HyperGraph()
    visit=[False]*(g.numNodes+1)
    visit_mark=[0]*(g.numNodes+1)

    num_sources=len(sources)

    for s in sources:
            visit[s]=True

    #for i in range(num_sources):
        #visit_mark[i]=sources[i]
    start = time.time()
    for i in range(num_live):
        hg.pollingLT2(g,sources,visit,visit_mark)
    
    nodesDegree=[0]*(g.numNodes+1)
    num=len(hg.edge_node)

    for i in range(num):
        len_sample=len(hg.edge_node[i])
        for j in range(len_sample):
            nodesDegree[hg.edge_node[i][j]]+=len_sample-j
    
    nodesDegree=np.array(nodesDegree)
    for source in sources:
        nodesDegree[source]=-1
    
    print(nodesDegree)
    #print(nodesDegree)
    #print(nodesDegree.shape)
    #print(nodesDegree.max())
    #print(hg.edge_node)
    #print(np.argmax(nodesDegree))
    A=[]
    numA=0
    times=[]
    while(numA<budget):
        uMax=np.argmax(nodesDegree)
        if(nodesDegree[uMax]<0):
            break
        A.append(uMax)
        #np.delete(nodesDegree,uMax)
        for i in range(num):
            if(uMax in hg.edge_node[i]):
                index=hg.edge_node[i].index(uMax)
                #print(index)
                len_sample=len(hg.edge_node[i])
                remove_nodes=hg.edge_node[i][index:]
                for j in range(index,len_sample):
                    nodesDegree[hg.edge_node[i][j]]-=len_sample-j
                
                for node in remove_nodes:
                    hg.edge_node[i].remove(node)
        
        nodesDegree[uMax]=-1
        numA+=1

        if(numA%10==0 and numA>0):
            #print(nodesDegree)
            end=time.time()-start
            times.append(end)
            writeFile=outFile+"seed_"+str(numA)+".sga"
            timeFile=outFile+"time_sga"+str(numA)+".txt"
            writetoFile(timeFile,times)
            writetoFile(writeFile,A)
        #print(A)
        #print(nodesDegree)
        
    #print(nodesDegree)
    #print(hg.edge_node)
        
def Degree(g, sources, budget, outFile):
    #g=Graph()

    degreeNodes=np.array(g.degree)
    for source in sources:
        degreeNodes[source]=-1
    
    A=[]
    times=[]

    start=time.time()

    sort_arr= np.argsort(degreeNodes)
    
    #print(sort_arr)

    for b in range(budget+1):
        if(b%10==0 and b>0):
            A=sort_arr[-b:]
            end=time.time()-start
            times.append(end)
            writeFile=outFile+"seed_"+str(b)+".dgree"
            
            writetoFile(writeFile,A)
    
    timeFile=outFile+"time_deg"+".txt"
    writetoFile(timeFile,times)

    print("Degree done!")
            #print(A)
            #print(len(A))

def Random(g,budget,sources, outFile):
    #g=Graph()
    #print(g.nodes)
    nodes=g.nodes
    #nodeRandom=np.array(g.nodes)
    start=time.time()
    times=[]
    randomNodes=np.array(nodes)

    for s in sources:
        nodes.remove(s)

    for b in range(budget+1):
        if(b>0 and b%10==0):
            index_A= np.random.randint(1,len(nodes),b)
            #print(index_A)
            A=randomNodes[index_A]
            end=time.time()-start
            times.append(end)

            writeFile=outFile+"seed_"+str(b)+".random"
            writetoFile(writeFile,A)
            
    timeFile=outFile+"time_random"+".txt"
    writetoFile(timeFile,times)

    print("Random done! ")

def read_source(file_name):
    f = open(file_name, 'r')
    data=f.readlines()
    sources=[]
    sources=[int(i.strip()) for i in data]
    return sources

def writetoFile(file_name,nodeList):
    f=open(file_name,'w')
    for node in nodeList:
        f.writelines(str(node)+"\n")
    f.close()

def main():
    data_name=sys.argv[1]
    #alg= sys.argv[2].strip()

    static_dir="../data"
    graph_file_name=static_dir+"/"+data_name+"/"+"network.txt"
    source_file=static_dir+"/"+data_name+"/"+"network.seeds"
    result_dir=static_dir+"/"+data_name+"/"+"result/"


    g=Graph()
    hg=HyperGraph()
    g.readGraphLT(graph_file_name)
    sources=read_source(source_file)

    print("Number of nodes: "+ str(g.numNodes))
    print("Number of edges: "+ str(g.numEdges))

    #Degree(g,sources,100,result_dir)
    #greedy(g,sources,10000,1, result_dir)
    #SGA(g,sources,100,100000,hg,result_dir)
    Random(g,100,sources,result_dir)
    

if __name__ == '__main__':
    main()

    

