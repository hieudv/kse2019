import numpy as np
import random
from joblib import Parallel, delayed
class Graph(object):

    def __init__(self):
        self.adjList =dict();
        self.weights=[];
        self.nodes=[]
        #self.degree=np.zeros(self.numNodes+1)
        self.numNodes=0;
        self.numEdges=0;
        self.degree=[]
        self.nodeCost=[]
        self.topics=[]
    
    def getWeight(self,u):
        return self.weight[u]
    
    def getDegree(self,u):
        return self.degree[u]
    
    def getEdge(self):
        pass
    
    def readGraphLT(self,file_name):
        f=open(file_name,"r")

        firt_line = f.readline()
        [n,m]=firt_line.split()
        self.numNodes=(int)(n)
        self.numEdges=(int)(m)

        weightR=dict()
        for i in range(self.numNodes+1):
            self.adjList[i]=[]
            #self.weights[i]=[]
            weightR[i]=[]
        self.degree=np.zeros(self.numNodes+1, dtype=int)
        #lines=f.readlines()
        for i in range(self.numEdges):
            e=f.readline().split(" ")
            #print(e)
            u=int(e[0].strip())
            v=int (e[1].strip())
            w=float(e[2].strip())
            self.adjList[u].append(v)
            weightR[u].append(w)
            self.degree[u]+=1
        
        for i in range(self.numNodes+1):
            tmp=weightR[i]
            for j in range(1,self.degree[i]):
                tmp[j]+=tmp[j-1]
            self.weights.append(tmp)
        self.nodes=list(self.adjList.keys())
    
    def readNodeCost(self, file_name):
        f = open(file_name,"r")
        data=f.readlines()
        self.nodeCost=[ float(cost.strip()) for cost in data]
    
    def readTopic(self, file_name):
        f = open(file_name,"r")
        for i in range(self.numNodes+1):
            line=f.readline()
            topic=[float(i.strip()) for i in line.split(" ")]
            self.topics.append(topic)

        
    def readGraphIC(file_name):
        pass
    
    def writeToFile(file_name):
        pass

class HyperGraph(object):

    def __init__(self):
        self.node_edge=[]
        self.edge_node=[]
        self.curEdge=0
        self.maxDegree=0
        self.numNodes=0
        self.numEdges=0
        self.hyper_degree=[]
    
    def randIndex_bin(self,weight_list, size):
        if(size<1):
            return -1

        ran=random.uniform(0,weight_list[size-1])

        if(ran>weight_list[size-1]):
            return -1
        
        left=1
        right=size-1
        prob=-1
        for i in range(size):
            prob=(int)((left+right)/2)
            if(weight_list[prob-1]>ran):
                right=prob-1
                continue
            if(weight_list[prob]<=ran):
                left = prob+1
                continue
            break
        return prob
    
    def randIndex_lin(self, weight_list, size):
        
        #print(ran)
        #print(weight_list)
        if(size<1):
            return -1

        ran=random.uniform(0,weight_list[size-1])

        if(ran>weight_list[size-1]):
            return -1
        
        if(size==1):
            if(ran<weight_list[size-1]):
                return 1
        else:
            for i in range(1,size+1):
                #print(weight_list[i-1])
                if(ran<weight_list[i-1]):
                    #print(i)
                    return i
        return -1


    def pollingLT(self, g, source, visit,visit_mark, num_sources):
        gSize=g.numNodes
        num_mark=num_sources
        cur=source
        #visit[cur]= True
        #visit_mark[num_mark-1]=cur
        for i in range(gSize):
            if(len(g.weights[cur])>32):
                ind=self.randIndex_bin(g.weights[cur], g.degree[cur])
            else:
                ind=self.randIndex_lin(g.weights[cur], g.degree[cur])
            
            if(ind==-1):
                break
            
            cur=g.adjList[cur][ind-1]
            #print("cur: "+ str(cur))
            
            if(visit[cur]==True): 
                break
            
            num_mark+=1
            visit[cur]=True
            visit_mark[num_mark-1]=cur
            #print("visit_mark: ")
            #print(visit_mark)
        self.edge_node.append(visit_mark[:num_mark])
        for i in range(1,num_mark):
            visit[visit_mark[i]]=False
        
    def pollingLT1(self, g, source_list,visit, visit_mark):
        #g=Graph()
        gSize=g.numNodes
        sSize=len(source_list)
        num_mark=sSize
        #print("sSize: ")
        #print(sSize)
        for source in source_list:
            cur=source
            for i in range(gSize):

                if(len(g.weights[cur])>=32):
                    ind=self.randIndex_bin(g.weights[cur],g.degree[cur])
                else:
                    ind = self.randIndex_lin(g.weights[cur], g.degree[cur])
                   
                if(ind==-1):
                    break
                cur=g.adjList[cur][ind-1]

                if(visit[cur]==True):
                    break

                visit_mark[num_mark]=cur
                visit[cur]=True
                num_mark+=1
        self.edge_node.append(visit_mark[:num_mark])
        self.hyper_degree.append(num_mark)
        for i in range(sSize,num_mark):
            visit[visit_mark[i]]=False
        
        return num_mark

    def pollingLT2(self, g, source_list,visit, visit_mark):
        gSize=g.numNodes
        #sSize=len(source_list)
        
        #print("sSize: ")
        #print(sSize)
        for source in source_list:
            cur=source
            #print("soures: ")
            #print(cur)
            num_mark=0
            for i in range(gSize):

                if(len(g.weights[cur])>=32):
                    ind=self.randIndex_bin(g.weights[cur],g.degree[cur])
                else:
                    ind = self.randIndex_lin(g.weights[cur], g.degree[cur])
                   
                if(ind==-1):
                    break
                cur=g.adjList[cur][ind-1]

                if(visit[cur]==True):
                    break

                visit_mark[num_mark]=cur
                visit[cur]=True
                num_mark+=1
            
            self.edge_node.append(visit_mark[:num_mark])
            self.hyper_degree.append(num_mark)
            for i in range(num_mark):
                visit[visit_mark[i]]=False
        
        #return num_mark


if __name__ == "__main__":
    g=Graph()
    g.readGraphLT("../test/network.txt")
    
    visit=[False]*(g.numNodes+1)
    visit_mark=[0]*(g.numNodes+1)
    sources=[3,6]
    for node in sources:
        visit[node]=True
    
    hg=HyperGraph()

    hg.pollingLT2(g,sources,visit,visit_mark)

    #print(g.adjList)
    #print(g.weights)
    #print (g.degree)
    print(hg.edge_node)
    
    
    